<?php



set_time_limit(0);
date_default_timezone_set("UTC"); 
ini_set('display_errors', 'on');


include 'config.php';
require_once "./Phpickle/phpickle.php";

	//Don't spam nickserv, it pisses off IRC staff
	$identified = 0;
	//510 - 64 is the true max message length but we're shaving off a few just incase.
	$max_message_length = 300; 
	$quiet_chan = array();
	$validated = array();
class IRCBot {

	//This is going to hold our TCP/IP connection

	var $socket;



	//This is going to hold all of the messages both server and client

	var $ex = array();
	var $notifylist = array();
	/*

	 Construct item, opens the server connection, logs the bot in



	 @param array

	*/

	function __construct($config)

	{

		$this->socket = fsockopen($config['server'], $config['port']);
		stream_set_timeout($this->socket, 0,5000);   // .5 seconds = 500000 microseconds
		$this->login($config);
		$this->gamenudge = socket_create(AF_INET, SOCK_STREAM, SOL_TCP); 
		socket_bind($this->gamenudge, "0.0.0.0", 45678);
		socket_listen($this->gamenudge);

		socket_set_option($this->gamenudge, SOL_SOCKET, SO_RCVTIMEO, array('sec'=>0, 'usec'=>5000));
		socket_set_option($this->gamenudge, SOL_SOCKET, SO_SNDTIMEO, array('sec'=>0, 'usec'=>5000));
		@socket_set_nonblock($this->gamenudge);		

		$this->main();


	}



	/*

	 Logs the bot in on the server



	 @param array

	*/

	function login($config)

	{

		
		if($config['irc_server_pass'] != '')
			$this->send_data('PASS', $config['irc_server_pass']);
		$this->send_data('USER', $config['nick'].' Ccomp5950 '.$config['nick'].' :'.$config['name']);

		$this->send_data('NICK', $config['nick']);

	}



	/*

	 This is the workhorse function, grabs the data from the server and displays on the browser

	*/

	function main()
	{
		global $config, $identified, $ops, $max_message_length, $quiet_chan, $validated;
		while(1==1)
		{
			
			socket_clear_error();
			$command = "none";
			$con = 0;
			$data="";
			if($this->socket != FALSE && $data = fgets($this->socket, 1024)) 
			{
				$con += 1;	
			}

			//Handles Reconnection.
			// $i is the timeout before it reconnects
			if($this->socket == false || feof($this->socket))
			{
				$connected = 0;
				while($connected == 0)
				{	
					if($this->reconnect()) $connected = 1;
				}
			}
			
			//Check the gamenudge socket for new information sent from server
			//We use phppickle to change it into a multidimensional array
			//then validate it against the game_password.
			$this->check_gamenudge();

			trim($data);
			if($config['debug']>=2 && $data)
			{
				echo $data. "\n";
			}

			//This section handles IRC data sent.
			if($con && $data != "")
			{

				$this->ex = explode(' ', $data);
				if(count($this->ex) && $this->ex[0] == 'PING')
				{
					$this->send_data('PONG', $this->ex[1]); //Plays ping-pong with the server to stay connected.
				}
	

				// If we've just connected to the server and nickserv complains that our NICK is registered and we haven't identified yet
				// Then go ahead and identify.  We set identified to 1 so we don't do this over and over again.
				else if(count($this->ex) > 1 && $identified == 0 && $this->ex[1] == '376' && is_null($config['nickserv_pass']))
				{
					$identified = 1;
					sleep(1);
					$this->join_channel($config['default_channel']);
					$this->join_channel(explode(",", $config['other_channels']));
				}

				else if(count($this->ex) > 1 && $identified == 0 && $this->ex[1] == 'NOTICE' && strtolower(substr($this->ex[0],1,8)) == "nickserv" && $config['nickserv_pass'] != '' )
	
				{
					$identified = 1;
					sleep(1);
					if($config['debug']) echo "sending ". $config['nickserv_pass'] ."\n";
						
					$this->identify($config['nickserv_pass']);
					sleep(2);
					$this->join_channel($config['default_channel']);
					$this->join_channel(explode(",", $config['other_channels']));
				}

				
				// Standard line reading, we figure out who sent the message, what the message is, and if they are in the OP list or not.
				else if(count($this->ex) >= 4)
				{
					$this->op = 0;
					$command = strtolower(str_replace(array(chr(10), chr(13)), '', $this->ex[3]));
					$this->cmd_chan = str_replace(array(chr(10), chr(13)), '', $this->ex[2]);
					$this->sender = str_replace(":","", strstr($this->ex[0], '!', true));
	
					if($this->cmd_chan == $config['nick'])
						$this->cmd_chan = $this->sender;

					if(in_array(strtolower($this->sender), $ops))
						if(array_key_exists(strtolower($this->sender), $validated) && $validated[strtolower($this->sender)] - time() < 900)
						{
							$this->op = 1;
						}
						else
						{
							$this->send_privmsg("nickserv", "STATUS $this->sender");
							sleep(1);
							$validate = fgets($this->socket, 1024);
							$validate = explode(' ', $validate);
							if($validate[3] == ":STATUS" && $validate[5] == "3")
								{
								$validated[strtolower($this->sender)] = time();
								$this->op = 1;
								}
							
						}
				}

				// For looking at NICK changes / QUITs
				else if(count($this->ex) == 3)
				{
					$this->sender = str_replace(":","", strstr($this->ex[0], '!', true));
				}


				// For when things don't match up how they should.  Used for debugging.
				else if(count($this->ex) < 3)
				{
					if($config['debug'] == 1)			
					{
						$error = "\n\nERROR: ";
						foreach ($this->ex as $key => $value)
							$error .= $key . " = " . $value . "  ";		
						echo $error . "\n";
					}
						$command = "shit";
					
				}

				if(count($this->ex) >= 3 && $this->ex[1] == 'NICK' && in_array(strtolower($this->sender), $this->notifylist))
                                {
                                        $newnick = strtolower(str_replace(":", "", $this->ex[2]));
                                        str_replace(strtolower($this->sender), $newnick, $this->notifylist);
                                        if($config['debug']) echo "Changing notify list:  ". $this->sender . " -> ". $newnick ."\n";
					$this->notifylist = array_unique($this->notifylist);
                                }
	
				switch($command) //List of commands the bot responds to from a user.

				{

					case ":!msg":
						if(count($this->ex) >= 6)
						{
							$cname = (strtolower($this->clean_string($this->ex[4])));
							$message = implode(" ", $this->clean_array($this->ex, 5));
							$message = trim($message);
							if(($this->op && $this->sender == $this->cmd_chan) || strtolower($this->cmd_chan) == strtolower($config['default_channel']))
							{
								$adminmsg = "?adminmsg=".$cname."&msg=".$message."&key=".$config['game_password']."&sender=".$this->sender;
	
								$data=$this->send_gameserver($adminmsg);
	
								$data = str_replace("\x00", "", $data);
								$data = trim($data);
								if($data != "Message Successful")
								$this->send_privmsg($this->cmd_chan , "Error: $data");
							
						
							}
						}
						else
						{
							if(($this->op && $this->sender == $this->cmd_chan) || strtolower($this->cmd_chan) == strtolower($config['default_channel']))
								$this->send_privmsg($this->cmd_chan , "USAGE:  !msg <cname> <message>" );
						}
						break;

					case ":!info":
						if(count($this->ex) == 5)
						{
							$cname = (strtolower($this->clean_string($this->ex[4])));
							if(($this->op && $this->sender == $this->cmd_chan) || strtolower($this->cmd_chan) == strtolower($config['default_channel']))
							{
								trim($cname);
								$query = "?notes=".$cname."&key=".$config['game_password'];
								$response = $this->send_gameserver($query);
								$response = str_replace("\x00", "", $response);
								$response = trim($response);

								if($config['debug']) echo "game server sent response: $response\n\n";

								if($response == "No information found on the given key.")
								{
									$this->send_privmsg($this->cmd_chan, "No notes for ". $cname);
								}
								else
								{
									$response = $this->send_privatepaste($response);
									if($response == "None") 
										$response = "Error, unable to get URL for uploaded notes";
									$this->send_privmsg($this->cmd_chan, "Private Paste of notes for ". $cname .": ". $response);
									
								}
							}
						}
						break;

					case ":!status":
						$query = "?status";
						$reply = $this->send_gameserver($query);
						//"version=Baystation12&mode=extended&respawn=1&enter=1&vote=1&ai=1&host&players=1&player0=Ccomp5950&admins=1"
						if(substr($reply,0,3) == "ver")
						{
                					parse_str($reply, $result);
							$this->send_privmsg($this->cmd_chan , "Players: ". $result['players'] . " and the mode is " . $result['mode'] . ". Station Time: ".$result['stationtime']);
						} 
						else 
						{
							$this->send_privmsg($this->cmd_chan , $reply);
						}
						break;

					case ":!players":
						$query = "?status";
						$reply = $this->send_gameserver($query);
						//"version=Baystation12&mode=extended&respawn=1&enter=1&vote=1&ai=1&host&players=3&stationtime=12%3a49&player0=Ccomp5950&player1=Ccomp5951&player2=Koosuthek&admins=2"
						if(substr($reply,0,3) == "ver")
						{
							$message = $this->format_players($reply);
							foreach($message as $msg)
							{
								if(strlen($msg))
									$this->send_privmsg($this->cmd_chan, $msg);
							}
						}
						else
						{
							$this->send_privmsg($this->cmd_chan , $reply);
						}
						break;

					case ":!admins":
						$query = "?status";
						$reply = $this->send_gameserver($query);
						if(substr($reply,0,3) == "ver")
						{
							parse_str($reply, $result);
							$this->send_privmsg($this->cmd_chan , "Admins: ". $result['admins']);
						}
						else
						{
							$this->send_privmsg($this->cmd_chan , $reply);
						}
						break;

					case ":!notify":
						if(in_array(strtolower($this->sender), $this->notifylist)) 
						{
						$this->send_privmsg($this->cmd_chan , $this->sender. ", you are already on the list to be notified.");
						}
						else
						{
						$this->send_privmsg($this->cmd_chan , $this->sender. ", you will be notified when the server restarts.");
						array_push($this->notifylist, strtolower($this->sender));
						}
						break;

					case ":!disable":
						if($this->op)
						{
							$this->send_privmsg($this->cmd_chan, chr(1) . "ACTION beeps and boops sadly". chr(1));
							$quiet_chan[] = $this->cmd_chan;
							$quiet_chan = array_unique($quiet_chan);
	
						}
						break;

					case ":!enable":
						if($this->op)
						{
							foreach($quiet_chan as $key => $val)
								if($val == $this->cmd_chan)
								{
									unset($quiet_chan[$key]);
									$this->send_privmsg($this->cmd_chan, chr(1) . "ACTION beeps and boops gladly". chr(1));
								}
						}
						break;
						

					case ":!leave":
						if($this->op)
						{
							$this->send_data("PART", $this->cmd_chan);
						}
						break;

					case ":!help":
						if(count($this->ex) == 4)
						{
							$reply = "I respond to !notify, !admins, !players, !status";
							if(strtolower($this->cmd_chan) == strtolower($config['default_channel']))
								$reply .= ", !msg <ckey>, !info <ckey> (player notes)";
							$this->send_privmsg($this->cmd_chan , "$reply.");
							break;
						}

					

				}//switch($command);

	
			}	//if(con && $data != "")
		} 	//while(1==1);

	} //function main();


	//This does the magic of sending data to the gameserver
	function send_gameserver($str)
	{
		global $config;
		// We're sending to the gameservers world/Topic() so all data needs to start with a ?
		if($str{0} != '?') $str = ('?' . $str);

		//This is basically a packet that was reverse engineered for sending to world/Topic() from the outside.
		//Pretty handy for what we're doing.		
		$query = "\x00\x83" . pack('n', strlen($str) + 6) . "\x00\x00\x00\x00\x00" . $str . "\x00";
	
		$server = socket_create(AF_INET,SOCK_STREAM,SOL_TCP) or die('Unable to create export socket.');
		if(!socket_connect($server,$config['game_addr'],$config['game_port'])) 
		{
			return "Unable to connect to server";
		}

		$bytestosend = strlen($query);
		$bytessent = 0;
		while ($bytessent < $bytestosend) 
		{
			$result = socket_write($server,substr($query,$bytessent),$bytestosend-$bytessent);
			if ($result===FALSE) die(socket_strerror(socket_last_error()));
			$bytessent += $result;
		}
	
		$result = socket_read($server, 100000, PHP_BINARY_READ);
		socket_close($server);
	
		if($result != "") 
		{
			if($result{0} == "\x00" || $result{1} == "\x83") 
			{
			
				$sizebytes = unpack('n', $result{2} . $result{3});
				$size = $sizebytes[1] - 1; 
			
				if($result{4} == "\x2a") 
				{ 
					$unpackint = unpack('f', $result{5} . $result{6} . $result{7} . $result{8});
					return $unpackint[1];
				}
				else if($result{4} == "\x06") 
				{
					$unpackstr = "";
					$index = 5;
				
					while($size > 0) 
					{
						$size--;
						$unpackstr = $unpackstr . $result{$index};
						$index++;
					}
					return $unpackstr;
				}
			}
		}		
	}

	function send_privatepaste($sendme)
	{
		global $config;
		$sendme = str_replace("%0D", "\n", $sendme);	//Un-encode it, then re-encode it.
		$sendme = str_replace("%0A", "\r", $sendme);
		$sendme = rawurlencode(utf8_encode($sendme));
		$post= "?_xsrf=&paste_content=$sendme&formatting=No+Formatting&line_numbers=off&expire=900&secure_paste=off&secure_paste_key=";
		$data_length = strlen($post);

		if($config['debug']) echo "Sending $post";

		$connection = fsockopen('privatepaste.com', 80);

		//sending the data
		fputs($connection, "POST /save HTTP/1.1\r\n");
		fputs($connection, "Host: privatepaste.com \r\n");
		fputs($connection, "Origin: http://privatepaste.com\r\n");
		fputs($connection, "Content-Type: application/x-www-form-urlencoded\r\n");
		fputs($connection, "Content-Length: ". $data_length."\r\n");
		fputs($connection, "Connection: close\r\n\r\n");
		fputs($connection, $post);

		$gotit=0;
		while($response = fgets($connection))
		{
			if($config['debug']) var_dump($response);
			if(strlen($response) >=8 && substr($response,0,8) == "Location")
			{	
				$gotit = 1;
				break;
			}
		}
		//closing the connection
		fclose($connection);
		if(!$gotit)
			return "None";
		$response = explode(" ", $response);
		$response = "http://privatepaste.com". $response[1];
		return $response;
		
	
	}

	function send_privmsg($chan, $msg = null)
	{
		global $quiet_chan;
		if((!empty($quiet_chan) && in_array($chan, $quiet_chan)) || is_null($msg))
			return;
		$this->send_data("PRIVMSG", $chan . " :". $msg);
		
	}

	function send_data($cmd, $msg = null) //Sends data to the IRC server
	{
		global $config;

		if($msg == null)

		{

			fputs($this->socket, $cmd."\r\n");

			if($config['debug']) echo $cmd;

		} else {

			fputs($this->socket, $cmd.' '.$msg."\r\n");

			if($config['debug']) echo $cmd ." ". $msg."\r\n";

		}

	}



	function join_channel($channel) //Joins a channel, used in the join function.

	{
		if(is_array($channel))

		{

			foreach($channel as $chan)

			{

				$this->send_data('JOIN', $chan);

			}

		} else {

			$this->send_data('JOIN', $channel);

		}

	}



	function protect_user($user = '')

	{

		if($user == '')

		{

				$user = strstr($this->ex[0], '!', true);

		}



		$this->send_data('MODE', $this->ex[2] . ' +a ' . $user);

	}

			
	function identify($password)
	{
		$this->send_data('PRIVMSG', 'nickserv :identify ' . $password);
	}


	function op_user($channel = '', $user = '', $op = true)
	{

		global $ops;

		if($channel == '' || $user == '')

		{

			if($channel == '')

			{

				$channel = $this->ex[2];

			}



			if($user == '')

			{

				$user = strstr($this->ex[0], '!', true);
			}
			

		}

		
		$clean_sender = str_replace(":","", strstr($this->ex[0], '!', true));
		$clean_user = str_replace(":","", $user);

		if($op)

		{
			if(in_array(strtolower($clean_sender), $ops))
			{
				$this->send_data('MODE', $channel . ' +o ' . $user);
			} 
			else
			{
				$this->send_data('PRIVMSG', $channel . " : Cannot op " . $clean_user . ", " . $clean_sender . " is not in my whitelist.");
			}

		} else {

			if(in_array(strtolower($clean_sender), $ops))
			{
				$this->send_data('MODE', $channel . ' -o ' . $user);
			}
			else
			{
				$this->send_data('PRIVMSG', $channel . " : Cannot deop " . $clean_user . ", " . $clean_sender . " is not in my whitelist.");
			}

		}

	}

	function clean_string($msg = ":,")
	{
		$msg = str_replace(":", "", $msg);
		$msg = str_replace(",", "", $msg);
		$msg = str_replace("\n", "", $msg);
		$msg = str_replace("\r", "", $msg);
		return $msg;
	}

	function clean_array($msg = null ,$start = 0, $stop = 0)
	{
		if(is_array($msg))
		{
			if(!$stop)
				$stop = count($msg)-1;

			$output = array_slice($msg, $start, $stop);
			return $output;
		}
		else
		{
			return;
		}
		
	}

	function check_gamenudge()
	{
		global $config, $gameips;
		$nudged = 0;
		socket_clear_error();
		if(($newc = @socket_accept($this->gamenudge)) !== false)
		{
			@socket_getpeername($newc, $ipaddress);			//Portscan check, they reconnect and disconnect real quick, $ipaddress will be null.
			if(!is_null($ipaddress) && $gamedata_raw = socket_read($newc, 4096))
			{
				if($config['debug']) var_dump($gamedata_raw);
				if(substr($gamedata_raw, 0, 4) == "(dp1")	//We check to see if someone is just messing with us this is the first 4 characters of data sent by nudge
				{
					$gamedata = phpickle::loads($gamedata_raw);
					//EXPECTED:  a multidimensional array with two main keys, ip and data, inside data will be the password, and the message.
					socket_close($newc);
					if($config['debug']) echo "received data from gamenudge:";
					if($config['debug']) var_dump($gamedata);
					if(array_key_exists("data", $gamedata)) 
					{
						$nudgeip   = trim($gamedata["ip"]);
						$nudgepass = trim($gamedata["data"][0]);
						if(count($gamedata["data"]) > 2)
						{
							$nudgedata = trim(implode(" ", array_slice($gamedata["data"], 1)));
						}
						else
						{
						$nudgedata = trim($gamedata["data"][1]);
						}
						if($nudgepass == $config['game_password'])
							$nudged = 1;
					}
				}
			}
		}
		 //This section handles gamenudge data
		if($nudged)
		{
			if(substr($nudgeip, 0,1) == "#")
			{
				$this->send_data("PRIVMSG", $nudgeip . " :". $nudgedata);
			}

			if(strpos($nudgedata, "round of") && strpos($nudgedata, "has ended"))
			{
				$i = 1;
				foreach($this->notifylist as $person)
				{
					$this->send_privmsg($person, "This is a notification that the current round has ended.  Restart in 60 seconds.");
					$i++;
					if($i == 4)
						{
						$i = 1;
						sleep(1);
						}
				}	
				$this->notifylist = array();
			}

		}


	}

	function format_players($reply = null)
	{
		global $max_message_length;
		if(is_null($reply)) return array();

		$i = 0;
		$j = 1;
		parse_str($reply, $result);
		$players = array();
		$message = array("Players: ". $result['players'] . " " ,"","");
		foreach($result as $key => $val)
		{
			if(substr($key,0,6) == "player" && $key != "players")
			{
				$players[] = $val;
			}
		}

		sort($players);
		foreach($players as $player)			
		{
			if(strlen($message[$i] . $player .", ") > $max_message_length)
			{
				if($i == 2)
				{
					$message[$i] .= "...";
					break;
				} else {
					$j = 1;
					$i++;
				}
			}
			if($j != 1)
			{
				$message[$i] .= ", ";
			}
			$message[$i] .= "(". substr($player, 0, 1) .")". substr($player, 1);
			$j++;
		}
		return $message;
	}

	function reconnect()
	{
		global $config, $identified;
		echo "ERROR: Lost connection to IRC server\n\n\n";
		$i = 30;
		while($i != 0)
		{
			echo "Reconnecting after ".$i." seconds...                   \r";
			$i--;
			sleep(1);
		}
		echo "                                                                    \r\n\n";
		if($this->socket != false)
		{
			socket_shutdown($this->socket);
			socket_close($this->socket);
		}
		if($this->socket = fsockopen($config['server'], $config['port']));
		{
			stream_set_timeout($this->socket, 0,5000);   // .5 seconds = 500000 microseconds
			$this->login($config);
			$identified = 0;
			return 1;
		}
		return 0;
	}

}

	echo "Starting BS12Bot\n\n";
	$bot = new IRCBot($config);
?>
