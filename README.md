#BS12Bot
Distributed under the "If you are a Dev for Baystation12 you are fully allowed to have the source" license.

This program is designed with the intent that it would be used with Baystation12 and anyone connected with that project is free to edit or view these files if they wish.

* * *

Features:

* Can send messages from gameadmins in an "admin channel" (#BS12Admin) to players in game.

* Can pull players notes and paste them into privatepaste supplying the requestor with a link to the pasted notes.

* Will answer status requests with actual station time.

* Will answer player requests with up to 3 lines worth of names all alphabatized.

* Will answer notify players of the round ending and track name changes of those asking to be notified.

* OP commands are authenticated against the players nick being registered and identified with nickserv through /msg nickserv STATUS <name>

* Admins commands are considered authenticated if performed within the admin channel.

* !enable and !disable respectivily shuts down it's output within the channel it is requested.

* Designed to work within a bouncer with multiple bots connected as well as a standalone bot.
